## begin fhead
#
# funcname "invert_img"
# name "Invert Colors"
# desc "Invert colors of an image"
#
## end fhead

import cv2


def invert_img(img, args):
    # TODO[FR]: bool imgHasAlpha as an arg_uiw_type?
    # -> borrow update_imgpreview() logic

    # if we bitwise_not() a (largely) opaque image with an alpha channel (all
    # alphas at 0xff), it will be (largely) transparent after inverting - we are
    # inverting colors, not all the bits in a pixel, so let's split the image
    # and invert just the color channels to achieve desired behaviour
    if (len(img.shape)) == 3:
        if img.shape[2] == 4:
            chans = cv2.split(img)
            chans[0] = cv2.bitwise_not(chans[0])
            chans[1] = cv2.bitwise_not(chans[1])
            chans[2] = cv2.bitwise_not(chans[2])
            tmp = cv2.merge(chans)
        else:
            # image is not grayscale, it is indexed or RGB, and has no alpha
            # channel
            tmp = cv2.bitwise_not(img)
    else:
        # image is grayscale
        tmp = cv2.bitwise_not(img)

    return tmp
