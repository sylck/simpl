# SIMPL

An image viewer application extensible with filter plug-ins.

Features a filter plug-in management system written from scratch. Comprising a
parsing and importing component, it has been developed with the aim of
imposing minimal obstruction to filter plug-in development efforts. Filter
developers are free to use any package or library to perform image
manipulation inside the filter, the only requirements are valid registration
and that the processed image matches input image in data type.

Application is written in Python and it is the scripting language used to
create filter plug-ins. Interface is developed using PySide, a library which
provides Python bindings for Qt. OpenCV is used to read and write images and
provide base image filtering functions to use or build upon in filter plug-ins.

## Screenshots

<img src="docs/initial.png" width="195"
title="Initial application state on start"/>&nbsp;
<img src="docs/jpegloaded.png" width="195"
title="JPEG image loaded"/>&nbsp;
<img src="docs/filter.png" width="195"
title="Filter dialog"/>&nbsp;
<img src="docs/filtersapplied.png" width="195"
title="Gaussian Blur and Invert Colors filters applied"/>&nbsp;
<img src="docs/transparency.png" width="195"
title="Handling images with transparency"/>

## Getting started

### Installation

The steps below describe the installation process on a machine running Ubuntu.
It should work with minimal changes on Debian, but it has not been tested.
Users running other Linux distributions will have to make some adjustments to
steps provided, primarily step 1.

1. Install system dependencies.
```bash
sudo apt install git python-pip cmake libqt4-dev
```

2. Install Python dependencies.
```bash
pip install PySide opencv-python
```

3. Clone this repository to a directory of your choice.
```bash
git clone https://gitlab.com/sylck/simpl.git
```

### Starting

Start the application by executing `main.py`.
```bash
./main.py
```

## License

SIMPL is licensed under the GNU General Public License, version 3. See
[LICENSE](LICENSE) for more information.
